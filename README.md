![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Camorra World Cheat

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Wat het is
De Camorra World cheat is een Google Chrome plugin die een groot aantal taken uit je handen neemt in de online text base maffia game [Camorra World].

## Hoe te downloaden
Maak een account aan in de [hansgoed.nl GitLab]. Ga naar het Camorra World Chrome Plugin project. Klik op releases en download de meest actuele versie.

## Ideeën
Als je een idee hebt of een probleem kun je in de [hansgoed.nl GitLab] een issue aanmaken. Probeer zo goed mogelijk te omschrijven wat je wilt of wat het probleem is.

## Bijdragen
Bijdragen zijn welkom. Kies een niet toegewezen issue. Fork de repo en ga aan de slag! Wanneer je klaar bent kun je een PR indienen.

[Camorra World]: http://www.camorraworld.nl
[hansgoed.nl GitLab]: https://git.hansgoed.nl